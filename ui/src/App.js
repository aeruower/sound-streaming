import { useState, useEffect } from "react"
import SongList from "./components/songlists/SongList";
import AudioPlayer from "react-h5-audio-player";
import Pages from "./components/Pages";
import "./sass.scss"
import SongEditor from "./components/SongEditor";
import PlaylistMenu from "./components/playlists/PlaylistMenu";

function App() {
  const ip = "http://localhost:5000/"
  // song object states
  const [songObjects, setSongObjects] = useState([]) // Background song objects, these dont change on page/playlist switch unless user clicks a song in them.
  const [viewSongObjects, setViewSongObjects] = useState([]) // The song objects seen by the user, change on every user page/playlist switch
  const [selectedSongObjects, setSelectedSongObjects] = useState(new Set())
  const [currentlyPlayingURL, setCurrentlyPlayingURL] = useState("")
  const [currentlyPlayingID, setCurrentlyPlayingID] = useState("")
  const [songPage,setSongPage] = useState(0)
  const [songPagesNum, setSongPagesNum] = useState(0)
  const [isSwitchingSongObjects, setIsSwitchingSongObjects] = useState(false)
  // playlist object states
  const [playlistObjects, setPlaylistObjects] = useState([])
  const [usingPlaylistID, setUsingPlaylistID] = useState("allsongs")

  const fetchSongObjects = async (page) => {
    console.log("fetching")
    const response = await fetch(`${ip}api/playlist/${usingPlaylistID}/${page}`);
    const newData = await response.json();
    setViewSongObjects(newData)
    console.log(newData)
    if(!songObjects.length > 0){
      setSongObjects(newData) // setting beggining song objects
    }
  };
  const fetchPlaylistObjects = async (page) => {
    const response = await fetch(`${ip}api/playlists/${page}`);
    const newData = await response.json();
    setPlaylistObjects(newData)
  };

  const fetchSongPageStats = async () => {
    const response = await fetch(`${ip}api/playlist/${usingPlaylistID}/pagestats`);
    const newData = await response.json();
    setSongPagesNum(newData.pages)
  };

  const fetchSongSearchResults = async (query) => {
    console.log(query)
    if(query.length>2){
      const response = await fetch(`${ip}api/playlist/allsongs/search/name/${query.toLowerCase()}`);
      const newData = await response.json();
      setViewSongObjects(newData)
    } else if (query.length === 0) {
      fetchSongObjects(songPage)
    }
  };

  useEffect(()=>{
    fetchSongPageStats();
    fetchPlaylistObjects(0);
  },[])

  useEffect(()=>{
    fetchSongObjects(songPage);
    fetchSongPageStats()
  },[songPage,usingPlaylistID])

  useEffect(()=>{
    setSongPage(0)
  },[usingPlaylistID])

  useEffect(()=>{
    if(songObjects.length > 0 && isSwitchingSongObjects){
      console.log("switching song objects")
      setCurrentlyPlayingID(songObjects[0].song_id)
      setIsSwitchingSongObjects(false)
    }
  },[viewSongObjects])
  
  useEffect(()=>{
    if(viewSongObjects.length>0){
      if(currentlyPlayingID.length > 0){
        setCurrentlyPlayingURL(`${ip}api/playlist/allsongs/song/${currentlyPlayingID}`)
      }
      if(songObjects.length>0){
        if(songObjects[0].song_id === viewSongObjects[0].song_id){
          document.getElementById(currentlyPlayingID).scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
        }
      }
    }
  },[currentlyPlayingID])

  const onPlaylistSearch = (query) =>{
    setPlaylistObjects(playlistObjects.filter((playlist)=>playlist.song_name.contains(query.toLowerCase())))
  }


  const onSongEnd = () =>{
    const curSongIndex = songObjects.findIndex((e)=>e.song_id === currentlyPlayingID)
    console.log(curSongIndex)
    if(curSongIndex+2 > songObjects.length){
      if(songPage+1 < songPagesNum){
        setSongPage(songPage+1)
        setIsSwitchingSongObjects(true)
      }
    } else {
      setCurrentlyPlayingID(songObjects[curSongIndex+1].song_id)
    }
  }
  return (
    <div className="p-2 d-flex flex-row justify-content-center gap-1">
      <PlaylistMenu playlistObjects={playlistObjects} usingPlaylistID={usingPlaylistID} setPage={setSongPage} setUsingPlaylistID={setUsingPlaylistID}/>
      <div className="d-flex flex-column">
        <SongList 
          songListName="All songs" 
          currentlyPlayingID={currentlyPlayingID} 
          songObjects={viewSongObjects} 
          selectedSongObjects={selectedSongObjects}
          setSelectedSongObjects={setSelectedSongObjects}
          onClickSong={(id)=>{setCurrentlyPlayingID(id);songObjects[0].song_id !== viewSongObjects[0].song_id && setSongObjects(viewSongObjects)}}
          onSearch={fetchSongSearchResults}
        />
        <AudioPlayer autoPlay={false} onEnded={()=>{onSongEnd()}} src={currentlyPlayingURL.length > 0 ? currentlyPlayingURL:""}/>
        {songPagesNum > 1 &&<Pages pagesNum={songPagesNum} page={songPage} setPage={setSongPage}/>}
      </div>
      {selectedSongObjects.size > 0 && <SongEditor ip={ip} isAlbum={false} selectedSongObjects={selectedSongObjects} updateSongs={()=>fetchSongObjects(songPage)} />}
    </div>

  );
}

export default App;
