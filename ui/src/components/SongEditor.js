import { Button,Alert } from 'react-bootstrap';
import { Trash, Download } from 'react-bootstrap-icons';
import { useState } from "react"
const SongEditor = ({ip,selectedSongObjects, isAlbum, updateSongs}) => {
    const [showDeleteAlert, setShowDeleteAlert] = useState(false)
    const downloadSelected = () =>{
        let downloadIDs = ""
        selectedSongObjects.forEach((object)=>{
            downloadIDs = downloadIDs + object.song_id + "&"
        })
        downloadIDs = downloadIDs.slice(0,downloadIDs.length-1)
        console.log(downloadIDs)
        window.open(`${ip}api/playlist/allsongs/song/download/${downloadIDs}`)
    }

    const deleteSelected = async () =>{
        let downloadIDs = ""
        selectedSongObjects.forEach((object)=>{
            downloadIDs = downloadIDs + object.song_id + "&"
        })
        downloadIDs = downloadIDs.slice(0,downloadIDs.length-1)
        await fetch(`${ip}api/playlist/allsongs/song/${downloadIDs}`,{method:"delete"})
        updateSongs()
    }
    return (
        <div>
            <div className={"d-flex p-2 m-2 flex-column gap-1 border"}>
                {showDeleteAlert &&<Alert variant="danger">
                    <Alert.Heading>Deleting songs!</Alert.Heading>
                        <p>
                            Are you sure you want to delete {selectedSongObjects.size} songs ? <br />
                            This action is irreversible.
                        </p>
                        <div className="d-flex justify-content-end gap-1">
                            <Button onClick={() => {setShowDeleteAlert(false);deleteSelected()}} variant="outline-danger">
                                Yes
                            </Button>
                            <Button onClick={() => setShowDeleteAlert(false)} variant="outline-secondary">
                                No, take me back!
                            </Button>
                        </div>
                    </Alert>
                }
                <h3>Selected: {selectedSongObjects.size}</h3>
                <div className={"d-flex p-2 flex-row gap-1 justify-content-center"}>
                    <Button variant="primary" onClick={()=>{downloadSelected();console.log("ssad")}} title="Download selected"><Download /><a href=""></a></Button>
                    <Button variant="danger" onClick={()=>{setShowDeleteAlert(true)}} title="Delete selected"><Trash /></Button>
                </div>
                <Button variant="secondary" disabled={!isAlbum}>Remove from playlist</Button>
            </div>
        </div>
    )
}

export default SongEditor
