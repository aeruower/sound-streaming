const Playlist = ({playlistObject,playlistIndex, setUsingPlaylistID, isSelected, setPage}) => {
    return (
        <div className="text-light bg-dark">
            <div 
            style={{overflow:"hidden", maxHeight:"3em"}} 
            className={`${isSelected?"bg-primary":"bg-dark"} song m-0 p-2 hover-shadow text-wrap text-light`}
            onClick={()=> {setUsingPlaylistID(playlistObject.playlist_id)}}>
                <div className="d-flex flex-row gap-2">
                    <span>{playlistIndex !== ""&&playlistIndex+1}</span><span>{playlistObject.playlist_name}</span>
                </div>
            </div>
            <hr style={{width:"100%",margin:"auto"}}/>
        </div>
    )
}

export default Playlist
