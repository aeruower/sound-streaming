# Self-hosted sound streaming web app
## Backend installation:
1. Create python virtual enviroment in the backend folder of the cloned repo.
    1. `python3 -m venv ./backend`
    2. Activate virtual enviroment
        - On windows run: `backend\\Scripts\\activate`
        - On linux: `source backend/bin/activate`
2. Install requirements.txt
    - `pip install -r backend/requirements.txt`
3. Run the development server:
    - Go to the backend directory
    - `flask run app.py`
