import re
from flask_restful import fields

"""

Database communicator
takes care of writing, deleting data from the database.
Provides finished resource fields and database searching.
Returning querries except for searching is not handeled by this file
as it most likely wont need to be changed later on.

"""


class ColumnDoesNotExist(Exception):
    def __init__(self, column_key) -> None:
        super().__init__(
            f"""Column with the name: {column_key} does not exist in the database."""
        )

    pass


class DBCommunicator:
    def __init__(self, db_object, table_object) -> None:
        self.db = db_object
        self.table = table_object
        self.columns = {
            key: item for key, item in vars(table_object).items() if key[0] != "_"
        }  # Filtering out variables with '_' at the start
        self.resource_fields = {
            column_name: fields.String for column_name in self.columns.keys()
        }

    """
    Returns serialization fields for the REST api
    dont_show_arr = an array of column keys not to be displayed
    """

    def get_resource_fields(self, dont_show_arr=[]) -> dict:
        if not dont_show_arr:
            return self.resource_fields
        else:
            return {
                key: data
                for key, data in self.resource_fields.items()
                if key not in dont_show_arr
            }

    def set_resource_field_type(self, column_name, rest_datatype) -> None:
        self.resource_fields[column_name] = rest_datatype

    def _print_columns(self) -> None:
        print(self.columns.keys())

    def _check_columns(self, data_dict) -> dict:
        entry = {}
        for key, data in data_dict.items():
            if key in self.columns.keys():
                entry[key] = data
            else:
                raise ColumnDoesNotExist(key)
        return entry

    def insert_data(self, data_dict) -> bool:
        entry = self._check_columns(data_dict)
        self.db.session.add(self.table(**entry))
        self.db.session.commit()
        return True

    def edit_data(self, data_dict, edited_object_id) -> bool:
        item = self.table.query.filter_by(song_id=edited_object_id)
        if item:
            entry = self._check_columns(data_dict)
            item.update(entry)
            self.db.session.commit()
        else:
            return False

    def delete_data(self, object):
        self.db.session.delete(object)
        self.db.session.commit()

    def search(self, columns: list, query: str, id_column_name: str):
        id_set = set()
        final_arr = []
        for column in columns:
            result = self.table.query.filter(
                self.columns[column].like(f"%{query}%")
            ).all()
            for id, object in {vars(id)[id_column_name]: id for id in result}.items():
                if id not in id_set:
                    id_set.add(id)
                    final_arr.append(object)

        return final_arr
