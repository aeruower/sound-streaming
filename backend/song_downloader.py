from uuid import uuid4
from pytube import YouTube, Playlist
import concurrent.futures
from os import rename
from uuid import uuid4
import pytube

class SongDownloader:
    def __init__(self):
        self.update_progress = [0,0]
        self.videos_dict = []

    def _downloader(self,video_object):
        id = uuid4()
        try:
            name = video_object.streams.filter(file_extension="webm", type="audio").desc().first().download("songs/")
            rename(f"{name}",f"songs/{id}.webm")
            self.update_progress[0] += 1
            self.videos_dict.append({"song_id":id,"song_name":video_object.title.split("#")[0]})
        except pytube.exceptions.AgeRestrictedError:
            pass
    
    def download_song(self,song_url) -> dict:
        yt = YouTube(f'{song_url}')
        self.videos_dict = []
        self.update_progress[0] = 0
        self.update_progress[1] = 0
        self._downloader(yt)
        return self.videos_dict[0]
        
    def download_playlist(self,playlist_url):
        pl = Playlist(playlist_url)
        self.videos_dict = []
        self.update_progress[1] = len(pl.videos)
        with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
            executor.map(self._downloader, pl.videos)
            executor.shutdown(wait=True)
        
        self.update_progress[0] = 0
        self.update_progress[1] = 0
        return self.videos_dict