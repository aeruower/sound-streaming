from flask import Flask, render_template
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
import uuid

from api.allsongs_playlist import allsong_playlist_func
from api.playlists import playlists_func
from api.albums import albums_func
from api.downloader import downloader_func
from db_com import DBCommunicator
from flask_cors import CORS


app = Flask(__name__, static_folder="static")
app.secret_key = f"{uuid.uuid4()}"
CORS(app)
app.config["SONGS_PER_PAGE"] = 32

# Database setup
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///test.db"
db = SQLAlchemy(app)


class SongsDB(db.Model):
    song_id = db.Column(db.String, unique=True, primary_key=True)
    song_name = db.Column(db.String, nullable=False)
    song_link = db.Column(db.String, nullable=False)
    song_author = db.Column(db.String, nullable=True)


class AlbumsDB(db.Model):
    album_id = db.Column(db.String, unique=True, primary_key=True)
    album_name = db.Column(db.String, nullable=False)
    album_author = db.Column(db.String, nullable=True)
    album_songs = db.Column(db.String, nullable=True)


class PlaylistsDB(db.Model):
    playlist_id = db.Column(db.String, unique=True, primary_key=True)
    playlist_name = db.Column(db.String, nullable=False)
    playlist_songs = db.Column(db.String, nullable=True)
    playlist_genre = db.Column(db.String, nullable=True)
    playlist_color = db.Column(db.String, nullable=True)


db.create_all()
songdb_com = DBCommunicator(db,SongsDB)
playlistdb_com = DBCommunicator(db,PlaylistsDB)

api = Api(app)
app.register_blueprint(allsong_playlist_func(api, songdb_com, SongsDB))
app.register_blueprint(playlists_func(api, db, PlaylistsDB, SongsDB))
app.register_blueprint(albums_func(api, db, AlbumsDB))
app.register_blueprint(downloader_func(api, db, SongsDB, PlaylistsDB))




@app.route("/")
def home():
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug="True", host="::")
