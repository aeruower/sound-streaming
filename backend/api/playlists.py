from flask_restful import Resource, fields, marshal_with, reqparse
from flask import Blueprint, request, current_app
from uuid import uuid4

PLAYLISTS_PER_PAGE = 32


def playlists_func(api, db, PlaylistsDB, SongsDB):
    playlists = Blueprint("playlists", __name__)
    playlist_resource_fields = {  # TODO make this global and imported from main app
        "playlist_id": fields.String,
        "playlist_name": fields.String,
        "playlist_genre": fields.String,
        "playlist_color": fields.String,
    }
    song_resource_fields = {
        "song_id": fields.String,
        "song_name": fields.String,
        "song_link": fields.String,
        "song_author": fields.String,
    }

    class Playlists(Resource):
        @marshal_with(playlist_resource_fields)
        def get(self, page=0):
            allplaylists = PlaylistsDB.query.all()
            if page == 0:
                return allplaylists[:PLAYLISTS_PER_PAGE]
            else:
                return allplaylists[
                    page * (PLAYLISTS_PER_PAGE) : (page + 1) * PLAYLISTS_PER_PAGE
                ]

    class Playlist(Resource):
        @marshal_with(song_resource_fields)
        def get(self, playlist_id, page):
            playlist_songs = SongsDB.query.filter(
                SongsDB.song_id.in_(
                    eval(
                        PlaylistsDB.query.filter_by(playlist_id=playlist_id)
                        .with_entities(PlaylistsDB.playlist_songs)
                        .all()[0][0]
                    )
                )
            ).all()
            if page == 0:
                return playlist_songs[: current_app.config["SONGS_PER_PAGE"]], 200
            else:
                return (
                    playlist_songs[
                        page
                        * (current_app.config["SONGS_PER_PAGE"]) : (page + 1)
                        * current_app.config["SONGS_PER_PAGE"]
                    ],
                    200,
                )

        def post(self):
            data = request.form.to_dict()
            try:
                # Editing
                # Change me if editing database
                id = data["playlist_id"]
                entry = PlaylistsDB.query.filter_by(playlist_id=id).first()
                if entry:
                    entry.playlist_name = data["playlist_name"]
                    entry.playlist_genre = data["playlist_genre"]
                    entry.playlist_color = data["playlist_color"]
                    entry.playlist_color = data["playlist_songs"]
                    db.session.add(entry)
                    db.session.commit()
                    return 200
                else:
                    return 404

            except KeyError:
                # Creating new playlist
                # Change me if editing database
                id = uuid4()
                entry = PlaylistsDB(
                    playlist_id=str(id),
                    playlist_name=data["playlist_name"],
                    playlist_genre=data["playlist_genre"],
                    playlist_color=data["playlist_color"],
                    playlist_songs=data["playlist_songs"],
                )
                db.session.add(entry)
                db.session.commit()
                return str(id), 200

        def delete(self, playlist_id):
            item = PlaylistsDB.query.filter_by(playlist_id=playlist_id).all()
            if item:
                for each in item:
                    db.session.delete(each)
                db.session.commit()
                return 200
            else:
                return "Playlist doesn't exist", 404

    class PlaylistSongPageStats(Resource):
        def get(self, playlist_id):
            playlist_songs = (
                PlaylistsDB.query.filter_by(playlist_id=playlist_id)
                .with_entities(PlaylistsDB.playlist_songs)
                .count()
            )
            return {
                "pages": int(
                    (playlist_songs / current_app.config["SONGS_PER_PAGE"]) + 0.99
                ),
                "songs": playlist_songs,
            }

    class Search(Resource):
        @marshal_with(playlist_resource_fields)
        def get(self, query):
            return PlaylistsDB.query.filter(
                PlaylistsDB.playlist_name.like(f"%{query}%")
            ).all()

    class PlaylistsPageStats(Resource):
        def get(self):
            return {
                "pages": int((PlaylistsDB.query.count() / PLAYLISTS_PER_PAGE) + 0.99),
                "playlists": PlaylistsDB.query.count(),
            }

    api.add_resource(Playlists, "/api/playlists/", "/api/playlists/<int:page>")
    api.add_resource(
        Playlist, "/api/playlist/", "/api/playlist/<string:playlist_id>/<int:page>"
    )
    api.add_resource(
        PlaylistSongPageStats, "/api/playlist/<string:playlist_id>/pagestats"
    )
    api.add_resource(Search, "/api/playlists/search/<string:query>")
    api.add_resource(PlaylistsPageStats, "/api/playlists/pagestats")
    return playlists
