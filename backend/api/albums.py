from flask_restful import Resource, fields, marshal_with, reqparse
from flask import Blueprint, make_response, send_file, request
from uuid import uuid4

ALBUMS_PER_PAGE = 32

def albums_func(api,db,albumsDB):
    albums = Blueprint("albums", __name__)
    album_resource_fields = {
        'album_id':fields.String,
        'album_name':fields.String,
        'album_songs':fields.String
    }

    class Albums(Resource):
        @marshal_with(album_resource_fields)
        def get(self,page=0):
            allalbums = albumsDB.query.all()
            if page == 0:
                return allalbums[:albumS_PER_PAGE]
            else:
                return allalbums[page*(albumS_PER_PAGE):(page+1)*albumS_PER_PAGE]
    
    class Album(Resource):
        @marshal_with(album_resource_fields)
        def get(self,album_id):
            return albumsDB.query.filter_by(album_id=album_id).all()
        
        def post(self):
            data = request.form.to_dict()
            try:
                # Editing
                # Change me if editing database
                id = data["album_id"]
                entry = albumsDB.query.filter_by(album_id=id).first()
                if entry:
                    entry.album_name = data["album_name"]
                    entry.album_genre = data["album_genre"]
                    entry.album_color = data["album_color"]
                    entry.album_color = data["album_songs"]
                    db.session.add(entry)
                    db.session.commit()
                    return 200
                else:
                    return 404

            except KeyError:
                # Creating new album
                # Change me if editing database
                id = uuid4()
                entry = albumsDB(album_id=str(id),album_name=data["album_name"],album_author=data["album_author"],album_songs=data["album_songs"])
                db.session.add(entry)
                db.session.commit()
                return str(id),200

        def delete(self,album_id):
            item = albumsDB.query.filter_by(album_id=album_id).all()
            if item:
                for each in item:
                    db.session.delete(each)
                db.session.commit()
                return 200
            else:
                return "album doesn't exist",404

    class AlbumSearch(Resource):
        @marshal_with(album_resource_fields)
        def get(self,query):
            return albumsDB.query.filter(albumsDB.album_name.like(f"%{query}%")).all()
    class AlbumsPageStats(Resource):
        def get(self):
            return {"pages":int((albumsDB.query.count()/albumS_PER_PAGE)+0.99), "albums":albumsDB.query.count()}
    
    api.add_resource(Albums,"/api/albums/","/api/albums/<int:page>")
    api.add_resource(Album,"/api/album/","/api/album/<string:album_id>")
    api.add_resource(AlbumSearch,"/api/albums/search/<string:query>")
    api.add_resource(AlbumsPageStats,"/api/albums/pagestats")
    return albums
